export class ClientProfile {
  creative_id: number;
  avatar: string;
  bio: string;
}
