export class User {
  //   constructor(id: number, username: string, email: string, user_type: number) {
  //     this.id = id;
  //     this.username = username;
  //     this.email = email;
  //     this.user_type = user_type;
  //   }

  id: number;
  username: string;
  email: string;
  user_type: number;
  token?: string;
}
