export class StudioProfile {
  studio_id: number;
  description: string;
  location: string;
  service_provided: string;
  advert_photos: object;
  logo: string;
  rates: string;
}
