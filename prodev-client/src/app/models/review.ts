export class Review {
  text: string;
  author: string;
  date: Date;
  showReview: boolean;
}
