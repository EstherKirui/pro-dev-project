export class Booking {
  creative_id: object;
  studio_id: object;
  email: string;
  session_duration: string;
  session_time: number;
}
